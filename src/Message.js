import React from 'react';
import TimeAgo from 'react-timeago';

export class Message extends React.Component {
  render() {
    let { color, author, text, time, myName } = this.props;
    let cl = (myName === author) ? 'my-bubble' : '';
    
    return (
      <div className={`chat-bubble--wrapper ${cl}`}>
        <div className="chat-bubble">
          <h5 style={{color: color}}> {author} </h5> 
          <p> { text } </p>
          <p className="timeago">
            <TimeAgo date={time} component="abbr" minPeriod="60" />
          </p>
        </div>
      </div>
    )
  }
}