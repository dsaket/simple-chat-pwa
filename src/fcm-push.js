import * as firebase from 'firebase';
import * as CONFIG from './config/index';

export function _initFCMPush(ws_client){
    if (!firebase.apps.length) {
      //initialize firebase app
      firebase.initializeApp(CONFIG.fcm_config);
    }
    
    // Retrieve Firebase Messaging object.
    const messaging = firebase.messaging(); 

    // grant permission to receive notifications in the browser
    messaging.requestPermission().then(function() {
      console.log('Notification permission granted.');
      // Retrieve an Instance ID token for use with FCM.
      _resetUI();
    }).catch(function(err) {
      console.log('Unable to get permission to notify.', err);
    });

    // Callback fired if Instance ID token is updated.
    messaging.onTokenRefresh(function() {
      messaging.getToken().then(function(refreshedToken) {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the app server.
        _setTokenSentToServer(false);
        // Send Instance ID token to app server.
        _sendTokenToServer(refreshedToken, ws_client);
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        _resetUI();
        // [END_EXCLUDE]
      })
      .catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
        // showToken('Unable to retrieve refreshed token ', err);
      });
    });
    
    function _resetUI() {
      messaging.getToken().then(function(currentToken) {
        if (currentToken) {
          console.log(currentToken);
          _sendTokenToServer(currentToken, ws_client);
          // updateUIForPushEnabled(currentToken);
        } else {
          // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');
          // Show permission UI.
          // updateUIForPushPermissionRequired();
          _setTokenSentToServer(false);
        }
      }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        // showToken('Error retrieving Instance ID token. ', err);
        _setTokenSentToServer(false);
      });
    }

    // Handle incoming messages. Called when: - a message is received while the app has focus
    // the user clicks on an app notification created by a sevice worker
    // messaging.setBackgroundMessageHandler` handler.
    messaging.onMessage(function(payload) {
      console.log("Message received. ", payload);
      _spawnNotification(payload.data.notification);
    });
    // [END receive_message]
}

const _isTokenSentToServer = function() {
  return window.localStorage.getItem(CONFIG.LS_KEYS.FCM_SENT_TO_SERVER) == 1;
}
const _setTokenSentToServer = function(sent) {
  window.localStorage.setItem(CONFIG.LS_KEYS.FCM_SENT_TO_SERVER, sent ? 1 : 0);
}
const _sendTokenToServer = function(currentToken, ws_client) {
  if (!_isTokenSentToServer()) {
    console.log('Sending token to server...');
    // Send the current token to your server.
    if (ws_client.readyState === ws_client.OPEN) {
      ws_client.send(JSON.stringify({
        type: CONFIG.TYPES.FCM_DATA,
        data: currentToken
      }));
    }
    _setTokenSentToServer(true);
  } else {
    console.log('Token already sent to server so won\'t send it again unless it changes');
  }
}
const _notify = function(data){
    console.log('data: ', data);
    // let notification = new Notification(data.title, {
    //   body: data.body,
    //   icon: data.icon,
    //   tag: 'Chat',
    //   vibrate: [200, 100, 400]
    // });
    // notification.onclick = function(event) {
    //   // prevent the browser from focusing the Notification's tab
    //   event.preventDefault(); 
    //   window.open(data.click_action, 'myWindow').focus();
    // }
}
const _spawnNotification = function(payload) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    console.warn("This browser does not support desktop notification");
  }
  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    _notify(payload);
  }
  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== "denied") {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        _notify(payload);
      }
    });
  }
}