export const serverUrl = 'wss://62542311.ngrok.io';
export const chat_placeholder = 'Enter chat message';
export const chat_placeholder_offline = 'You\'re currently offline';
export const nickname_placeholder = 'Enter your nickname';
export const fcm_config = {
	apiKey: "AIzaSyAds5kps8VYi7IoN4wIQ8N_go6wTEv3Qb4",
	authDomain: "chat-pwa-app.firebaseapp.com",
	databaseURL: "https://chat-pwa-app.firebaseio.com",
	projectId: "chat-pwa-app",
	storageBucket: "chat-pwa-app.appspot.com",
	messagingSenderId: "500248860563"
};
export const TYPES = {
	USER_LOGIN: 'USER_LOGIN',
	CHAT_MESSAGE: 'CHAT_MESSAGE',
	FCM_DATA: 'FCM_DATA',
	REMOVE_FCM_DATA: 'REMOVE_FCM_DATA'
};
export const LS_KEYS = {
	USER_ID: 'USER_ID',
	FCM_SENT_TO_SERVER: 'FCM_SENT_TO_SERVER',
	CHAT_HISTORY: 'CHAT_HISTORY'
}
export const NICKNAME_ANTI_PATTERN = /[^a-zA-Z0-9-_]/;
export const TOAST_STYLE = {
  container: { // container style 
    position: 'fixed',
    top: 'auto',
    bottom: '12%',
    width: '100%',
    zIndex: 1000,
    textAlign: 'center'
  },
  text: { // text style 
    display: 'inline-block',
    maxWidth: '90%',
    margin: '0 3%',
    padding: '10px',
    fontSize: '14px',
    lineHeight: 'normal',
    color: '#fff',
    background: '#656565',
    borderRadius: '25px',
  },
}; // custom style