import React from 'react';
import * as CONFIG from './config/index';
import * as FCM_PUSH from './fcm-push';
import { toast } from 'react-android-style-toast';

import { Message } from './Message';
import arrow_back from './img/ic_arrow_back_24px.svg';
import paper_rocket_submit from './img/send-message_red.png';
import './App.css';

var W3CWebSocket = require('websocket').w3cwebsocket;

var types = CONFIG.TYPES;

export class App extends React.Component {
  constructor(props) {
    super(props);
    var USER_ID = localStorage.getItem(CONFIG.LS_KEYS.USER_ID);
    var history = JSON.parse(localStorage.getItem(CONFIG.LS_KEYS.CHAT_HISTORY));
    this.state = {
      inputDisable: false,
      statusText: 'Connecting...',
      statusWorking: true,
      inputText: '',
      myName: USER_ID ? USER_ID : undefined,
      history: history ? history : [],
      errorNick: ''
    };
    this.toastStyle = CONFIG.TOAST_STYLE;
    
    this._handleOpen = this._handleOpen.bind(this);
    this._handleClose = this._handleClose.bind(this);
    this._handleError = this._handleError.bind(this);
    this._handleMessage = this._handleMessage.bind(this);
    this._handleInput = this._handleInput.bind(this);
    this._handleExitFromChat = this._handleExitFromChat.bind(this);
  }
  
  _checkWSConnection() {
    this.client = new W3CWebSocket(CONFIG.serverUrl);
    this.client.onerror = this._handleError;
    this.client.onopen = this._handleOpen;
    this.client.onclose = this._handleClose;
    this.client.onmessage = this._handleMessage;  
  }
  componentDidMount() {
    this._checkWSConnection();
    this.main_el = document.querySelector(".main--content main");
  }
  componentDidUpdate() {
    this.main_el.scrollTop = this.main_el.scrollHeight;
  }

  _handleError(error) {
    console.log('error occured');
    console.log('web socket connection error: switching to offline mode');
    
    this.setState({
      statusWorking: false
    });
  }
  _handleOpen() {
    console.log('WebSocket Client Connected');
    toast.show("WebSocket Client Connected", 3000, this.toastStyle);

    if(this.state.myName){
      this.client.send(JSON.stringify({
        type: types.USER_LOGIN,
        data: this.state.myName
      }));
    }
    this.setState({
      inputDisable: false,
      statusText: 'Choose name:',
      statusWorking: true
    });
  }
  _handleClose() {
    console.log('Client Closed');
    
    var _self = this;
    this.timeout = setTimeout(function(){
      _self._checkWSConnection();
    }, 5000)
  }
  _handleMessage(message) {
    try {
      var json = JSON.parse(message.data);
    } catch (e) {
      console.log('Invalid JSON: ', message.data);
      return;
    }

    switch(json.type) {
      case 'color':
        // from now user can start sending messages
        toast.show("Welcome " + this.state.myName + "!", 3000, this.toastStyle);
        FCM_PUSH._initFCMPush(this.client);
        this.setState((oldState) => ({
          inputDisable: false,
          statusText: `${oldState.myName}:`,
          myColor: json.data
        }));
        break;
      case 'history':
        // insert every single message to the chat window
        localStorage.setItem(
          CONFIG.LS_KEYS.CHAT_HISTORY, 
          JSON.stringify(json.data)
        );
        this.setState({
          inputDisable: false,
          history: json.data.slice()
        });
        break;
      case 'message':
        // let the user write another message
        localStorage.setItem(
          CONFIG.LS_KEYS.CHAT_HISTORY, 
          JSON.stringify(this.state.history.concat([json.data]))
        );
        this.setState((oldState) => ({
          inputDisable: false,
          history: oldState.history.concat([json.data])
        }));
        break;
      case 'spam':
        //user is spamming the app
        toast.show("It seems like you are spamming!", 3000, this.toastStyle);
        this.setState({
          inputDisable: false
        });
        break;
      default:
        this.setState({
          inputDisable: false
        });
        console.log('Unknown data type');
    }
  }
  
  _handleInput(e) {
    this.setState({
      inputText: e.target.value
    });
  }
  _handleSubmit(type,e) {
    e.preventDefault();
    
    var msg = this.state.inputText;
    if(!msg){
      toast.show("Input field is empty", 3000, this.toastStyle);
      return;
    }
    
    if(type === types.USER_LOGIN){
      if((CONFIG.NICKNAME_ANTI_PATTERN).test(msg)){
        toast.show("*Only alphanumeric characters, (-) and (_) allowed", 3000, this.toastStyle);
        return;
      }
    }
    
    var newState = {
      inputText: '',
      inputDisable: true,
      errorNick: ''
    };
    if (this.state.myName === undefined) {
      newState.myName = msg;
      localStorage.setItem(CONFIG.LS_KEYS.USER_ID,msg);
    }
    
    if (this.client.readyState === this.client.OPEN) {      
      this.client.send(JSON.stringify({
        type: type,
        data: msg
      }));
    }else{
      newState.statusWorking = false;
    }
    this.setState(newState);
  }
  _handleExitFromChat() {
    // if(window.confirm("Are you sure you want to log out?")){
      localStorage.removeItem(CONFIG.LS_KEYS.USER_ID);
      localStorage.removeItem(CONFIG.LS_KEYS.FCM_SENT_TO_SERVER);
      this.setState({
        inputDisable: false,
        myName: undefined
      });
      this.client.send(JSON.stringify({
        type: types.REMOVE_FCM_DATA,
        data: ''
      }));
    // }
  }
  componentWillUnmount() {
    clearTimeout(this.timeout);
    this.client.close();
  }

  render() {
    var hideCl = (this.state.myName === undefined) ? 'hide-panel' : '';
    var offlineCl = (this.state.statusWorking) ? '' : 'offline';
    var chat_placeholder = (this.state.statusWorking) ? CONFIG.chat_placeholder : CONFIG.chat_placeholder_offline;
    var history = this.state.history || [];
    var empty_state_class = (history.length == 0) ? 'empty_state' : '';

    return (
      <div className="container">
        <section className="fixed--container flex--column entry--content">
          <main>
            <form className="text-center" onSubmit={this._handleSubmit.bind(this,types.USER_LOGIN)}>
              <input type="text" value={this.state.inputText} onChange={this._handleInput} disabled={this.state.inputDisable} placeholder={CONFIG.nickname_placeholder} />
              {
                this.state.errorNick && (<p className="error"> {this.state.errorNick} </p>)
              }
              <div className="action--button">
                <button type="submit">Join Chat Room</button>
              </div>
            </form>
          </main>
        </section>
        
        <section className={`fixed--container main--content ${offlineCl} ${hideCl}`}>
          <header>
            <a className="top-left-abs" onClick={this._handleExitFromChat}>
              <img src={arrow_back} alt="back" className="ver-centered"/>
            </a>
            Chat Room
          </header>
          <main className={empty_state_class}>
            {
              history.map((message,index) => <Message key={index} myName={this.state.myName} {...message} /> )
            }
          </main>
          <footer>
            <form onSubmit={this._handleSubmit.bind(this,types.CHAT_MESSAGE)}>
              <input type="text" value={this.state.inputText} onChange={this._handleInput} disabled={this.state.inputDisable || !this.state.statusWorking} placeholder={chat_placeholder} />
              <a onClick={this._handleSubmit.bind(this,types.CHAT_MESSAGE)}>
                <img src={paper_rocket_submit} alt="submit" className="ver-centered" />
              </a>
            </form>
          </footer>
        </section>
        {/*
          JSON.stringify(this.state, 2, null)
        */}
      </div>
    );
  }
}