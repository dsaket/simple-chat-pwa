"use strict";
process.title = 'node-chat';

// constants
var webSocketsServerPort = 1337;
var API_SERVER_KEY = "AAAAdHkn15M:APA91bHERYyYOC-Lhtg7IDhWbtxN2msra_0HeDd-_hApmUTrNt33-MDVRmGGNilI6WykdKIRVw69feIIZAue4lA5B0Yx0dgXQasR23clgb-yCSm6dj6AHh3E16Hph8JNrK0T_C_qb9lV";
var db_url = "mongodb://localhost:27017/app_data";
var user_collection = "user_data";
var chat_collection = "chat_data";
var LIMIT_CHAT_HISTORY = 1000;
var SPAMMING_TIME_CONSTRAINT = 5000; //miliseconds
var SPAMMING_MAX_ATTEMPT = 3;

// websocket, http servers, request, mongo client, assert
var webSocketServer = require('websocket').server;
var http = require('http');
var requestLib = require('request');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');


/**
 * Global variables
 */
// list of currently connected clients (users)
var clients = [];
// list of all the users in database
var users = [];

// Array with some colors
var colors = [ 'red', 'green', 'blue', 'magenta', 'purple', 'plum', 'orange' ];
// ... in random order
colors.sort(function(a,b) { return Math.random() > 0.5; } );

/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
  
});
server.listen(webSocketsServerPort, function() {
  console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
  httpServer: server
});
// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
  console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
  
  // accept connection - you should check 'request.origin' to
  // make sure that client is connecting from your website
  // (http://en.wikipedia.org/wiki/Same_origin_policy)
  var connection = request.accept(null, request.origin); 
  
  // we need to know client index to remove them on 'close' event
  var index = clients.push(connection) - 1;
  var userName = false;
  var userColor = false;
  console.log((new Date()) + ' Connection accepted.');
  
  // send back chat history
  MongoClient.connect(db_url, function(err, db) {
    assert.equal(null, err);
    
    db.collection(chat_collection).find({}).toArray(function(err, res) {
      assert.equal(null, err);
      var history = res.slice((-1)*LIMIT_CHAT_HISTORY) || [];
      connection.sendUTF(JSON.stringify({ 
        type: 'history', 
        data: history
      }));
    });

    db.collection(user_collection).find({}).toArray(function(err, res) {
      assert.equal(null, err);
      users = res.slice();
    });
  });
  

  // user sent some message
  connection.on('message', function(message) {
    if (message.type === 'utf8') { // accept only text
      
      var payload = JSON.parse(message.utf8Data);
      console.log(payload);
      var msg = _htmlEscape(payload.data);
      
      if (payload.type === 'USER_LOGIN') {
        userName = msg;

        MongoClient.connect(db_url, function(err, db) {
          assert.equal(null, err);
          //check if user already exists
          db.collection(user_collection).findOne({user_id: userName}, function(err, res) {
            assert.equal(null, err);
            console.log(res);
            if(res){
              //use previous data
              userColor = res.color;
              db.close();
            }else{
              //create and insert new user
              userColor = colors.shift();
              var myobj = {
                user_id: userName,
                color: userColor
              }
              db.collection(user_collection).insertOne(myobj, function(err, res) {
                assert.equal(null, err);
                console.log("1 document inserted");
                users = users.concat([myobj]);
                db.close();
              });
            }
            //broadcast to client
            connection.sendUTF(JSON.stringify({ 
              type: 'color', 
              data: userColor 
            }));
            console.log((new Date()) + ' User is known as: ' + userName + ' with ' + userColor + ' color.');
          });
        });
      }

      else if (payload.type === 'CHAT_MESSAGE') { 
        // log and broadcast the message
        console.log((new Date()) + ' Received Message from ' + userName + ': ' + message.utf8Data);
        
        // we want to keep history of all sent messages
        var myobj = {
          time: (new Date()).getTime(),
          text: msg,
          author: userName,
          color: userColor
        };
        
        MongoClient.connect(db_url, function(err, db) {
          assert.equal(null, err);

          //check whether user is spamming
          var spam_lower_lange = myobj.time - SPAMMING_TIME_CONSTRAINT;
          db.collection(chat_collection).find({
            author:userName,
            time: { $gt: spam_lower_lange }
          }).toArray(function(err, res) {
            assert.equal(null, err);
            if(res.length >= SPAMMING_MAX_ATTEMPT){
              //broadcast to client
              connection.sendUTF(JSON.stringify({ 
                type: 'spam',
                data: {}
              }));
              db.close();
              console.log("Spammer detected");
            }else{
              //insert new chat data
              db.collection(chat_collection).insertOne(myobj, function(err, res) {
                assert.equal(null, err);
                console.log("1 document inserted");
                
                // broadcast message to all connected clients
                var json = JSON.stringify({ 
                  type:'message', 
                  data: myobj 
                });
                for (var i=0; i < clients.length; i++) {
                  clients[i].sendUTF(json);
                }
                users.forEach(function(user){
                  if(user.fcm_id && user.user_id !== userName){
                    _sendToGCMServer({
                        title: "New message from " + userName,
                        body: msg,
                        icon: "/favicon.ico",
                        tag: 'Chat',
                        // vibrate: [200, 100, 400],
                        click_action: "/"
                      }, user.fcm_id
                    );
                  }
                });

                db.close();
              });
            }
          });
        });
      }

      else if (payload.type === 'FCM_DATA') {
        var myobj = {
          user_id: userName,
          fcm_id: msg
        };

        MongoClient.connect(db_url, function(err, db) {
          assert.equal(null, err);
          //upsert fcm data in user data collection
          db.collection(user_collection).update({user_id: userName}, {$set: myobj}, {upsert: true}, function(err, res) {
            assert.equal(null, err);
            console.log("1 document upserted");
            users.forEach(function(user){
              if(user.user_id === userName){
                user.fcm_id = msg;
              }
            });
            db.close();
          });
        });
      }

      else if (payload.type === 'REMOVE_FCM_DATA') {
        var myobj = {
          fcm_id: 1
        };

        MongoClient.connect(db_url, function(err, db) {
          assert.equal(null, err);
          //remove fcm data from user data collection
          db.collection(user_collection).update({user_id: userName}, {$unset: myobj}, function(err, res) {
            assert.equal(null, err);
            console.log("update query has run");
            users.forEach(function(user){
              if(user.user_id === userName){
                delete user.fcm_id;
              }
            });
            db.close();
          });
        });
      }

    }
  });
  

  // user disconnected
  connection.on('close', function(connection) {
    if (userName !== false && userColor !== false) {
      console.log((new Date()) + " Peer " + connection.remoteAddress + " disconnected.");
      // remove user from the list of connected clients
      clients.splice(index, 1);
      // push back user's color to be reused by another user
      colors.push(userColor);
    }
  });
});


/*****************************
      Helper functions
*****************************/
function _htmlEscape(str) {
  return String(str);
      // .replace(/&/g, '&amp;').replace(/</g, '&lt;')
      // .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function _sendToGCMServer(payload, fcm_id){
  requestLib({ 
    url: 'https://fcm.googleapis.com/fcm/send',
    method: "POST",
    json: true,
    headers: {
      Authorization: "key="+API_SERVER_KEY
    },
    body: { 
      data: {
        notification: payload
      },
      to: fcm_id
    }
  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
        console.log(body);
    }
  });
}