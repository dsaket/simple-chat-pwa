importScripts('https://www.gstatic.com/firebasejs/4.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.2.0/firebase-messaging.js');

if (!firebase.apps.length) {
	firebase.initializeApp({
	  'messagingSenderId': '500248860563'
	});
}

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	console.log('[firebase-messaging-sw.js] Received background message ');

	var data = payload.data.notification;
	if(typeof data == "string"){
		data = JSON.parse(payload.data.notification);
	}
	console.log('data: ', data);
	// Customize notification here
	return self.registration.showNotification(data.title, {  
      body: data.body,
      icon: data.icon,
      tag: data.tag,
      vibrate: [200, 100, 400],
      data: data.click_action
    });
});

self.addEventListener('notificationclick', function(event) {  
  console.log('data: ', event);
  event.notification.close();
  
  if('data' in event.notification){
    var temp = event.notification.data;
    if ( 'openWindow' in clients ) {
      clients.openWindow(temp);
    }
  }
});